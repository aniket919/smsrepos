package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.model.Student;

public class StudentDao {

	private String url;
	private String usrName;
	private String passwrd;
	private Connection con;
	
	public StudentDao(String url, String usrName, String passwrd) {
		this.url = url;
		this.usrName = usrName;
		this.passwrd = passwrd;
	}
	
	protected void connect() throws SQLException{
		if((con == null) || (con.isClosed())){
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				throw new SQLException(e);
			}
			con = DriverManager.getConnection(url, usrName, passwrd);
		}
	}
	
	protected void disConnect() throws SQLException{
		if((con != null) && (!con.isClosed())) {
			con.close();
		}
	}
	
	public boolean insertStudent(Student student) throws SQLException{
		String sql = "insert into smsprime (name,branch,batch) values (?,?,?)";
		connect();
		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setString(1, student.getName());
		stmt.setString(2, student.getBranch());
		stmt.setString(3, student.getBatch());
		
		boolean rowInserted = stmt.executeUpdate() > 0;
		stmt.close();
		disConnect();
		return rowInserted;
	}
	
	public List<Student> getAllStudent() throws SQLException{
		List<Student> studentList = new ArrayList<>();
		String sql = "select * from smsprime";
		
		connect();
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()) {
			int rollNo = rs.getInt("rollNo");
			String name = rs.getString("name");
			String 	branch = rs.getString("branch");
			String batch = rs.getString("batch");
			
			Student student = new Student(rollNo, name, branch, batch);
			studentList.add(student);
		}
		rs.close();
		stmt.close();
		disConnect();
		return studentList;
	}
	
	
	public boolean deleteStudent(Student student) throws SQLException{
		String sql = "delete from smsprime where rollNo = ?";
		connect();
		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setInt(1, student.getRollNo());
		
		boolean rowDeleted = stmt.executeUpdate() > 0;
		stmt.close();
		disConnect();
		return rowDeleted;
	}
	
	public boolean updateStudent(Student student) throws SQLException{
		
		String sql = "update smsprime set name = ?, branch = ?, batch = ?";
		sql += " where rollNo = ?";
		connect();
		
		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setInt(1, student.getRollNo());
		stmt.setString(2, student.getName());
		stmt.setString(3, student.getBranch());
		stmt.setString(4, student.getBranch());
		
		boolean rowUpdated = stmt.executeUpdate() > 0;
		stmt.close();
		disConnect();
		return rowUpdated;
	}
	
	public Student getStudent(int rollNo) throws SQLException{
		Student student = null;
		String sql = "select name,branch,batch from smsprime where rollNo = ?";
		connect();
		
		PreparedStatement stmt = con.prepareStatement(sql);
		stmt.setInt(1, rollNo);
		
		ResultSet rs = stmt.executeQuery();
		
		if(rs.next()) {
			
			String name = rs.getString("name");
			String branch = rs.getString("branch");
			String batch = rs.getString("batch");
			
			student = new Student(rollNo, name, branch, batch);
		}
		rs.close();
		stmt.close();
		return student;
	}
}
