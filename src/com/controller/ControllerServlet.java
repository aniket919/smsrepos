package com.controller;


import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDao;
import com.model.Student;

public class ControllerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private StudentDao studentDao;

	public void init() {
		String jUrl = getServletContext().getInitParameter("url");
		String jUsername = getServletContext().getInitParameter("usrName");
		String jPasswprd = getServletContext().getInitParameter("passwrd");

		studentDao = new StudentDao(jUrl, jUsername, jPasswprd);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException ,IOException {
		String action = req.getServletPath();
		System.out.println("action came : " + action);
		
		try {
			switch(action) {
			case "/new":
				System.out.println("request came here.........");
				showNewForm(req, res);
				break;
				
			case "/insert":
				insertStudent(req, res);
				break;
				
			 case "/delete":
		        deleteStudent(req, res);
		            break;
		            
		     case "/edit":
		        showEditStudent(req, res);
		         	break;
		        
		     case "/update":
		        updateStudent(req, res);
		            break;
		            
		     default:
		        listStudent(req, res);
		            break;
			}
			
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	private void listStudent(HttpServletRequest req, HttpServletResponse res) throws SQLException, IOException, ServletException{
		
		List<Student> studentList = studentDao.getAllStudent();
		req.setAttribute("studentList",studentList);
		RequestDispatcher rd = req.getRequestDispatcher("StudentList.jsp");
		rd.forward(req, res);
		
	}
	
	private void showNewForm(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		
		RequestDispatcher rd = req.getRequestDispatcher("StudentForm.jsp");
		rd.forward(req, res);
	}
	
	private void insertStudent(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException, SQLException{
		String name = req.getParameter("name");
		String branch = req.getParameter("branch");
		String batch = req.getParameter("batch");
		
		Student student = new Student(name, branch, batch);
		studentDao.insertStudent(student);
		res.sendRedirect("list");
	}
	
	private void deleteStudent(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException, SQLException{
		int rollNo = Integer.parseInt(req.getParameter("rollNO"));
		Student student = new Student(rollNo);
		
		studentDao.deleteStudent(student);
		res.sendRedirect("list");
	}
	
	private void showEditStudent(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException, SQLException{
		String id = req.getParameter("idN");
		System.out.println("printing roll number " + id);
		int rollNo = 0;
		if(id != null) {
			rollNo = Integer.parseInt(id);
		}
		//int rollNo = Integer.parseInt(req.getParameter("rollNo"));
		System.out.println("this is debug..." + rollNo);
		Student existingStudent = studentDao.getStudent(rollNo);
		
		RequestDispatcher rd = req.getRequestDispatcher("StudentForm.jsp");
		req.setAttribute("student", existingStudent);
		rd.forward(req, res);
	}

	private void updateStudent(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException, SQLException{

		 	int rollNo = Integer.parseInt(req.getParameter("rollNo"));
	        String name = req.getParameter("name");
	        String branch = req.getParameter("branch");
	        String batch = req.getParameter("batch");
	        Student student = new Student(rollNo, name, branch, batch);
	        studentDao.updateStudent(student);
	        res.sendRedirect("list");
	}
}
