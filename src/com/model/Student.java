package com.model;

public class Student {

	private int rollNo;
	private String name;
	private String branch;
	private String batch;
	
	public Student() {
	}
	
	public Student(int rollNo) {
		this.rollNo = rollNo;
	}
	
	public Student(String name, String branch, String batch) {
		this.name = name;
		this.branch = branch;
		this.batch = batch;
	}
	
	
	public Student(int rollNo, String name, String branch, String batch) {
		this(name, branch, batch);
		this.rollNo = rollNo;
	}

	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", name=" + name + ", branch=" + branch + ", batch=" + batch + "]";
	}
	
	
	
}
