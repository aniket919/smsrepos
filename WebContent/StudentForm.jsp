<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add new student</title>
</head>
<body>
	<div align="center">
		<h1>Books Management</h1>
		<h2>
			<a href="/new">Add New Book</a> &nbsp;&nbsp;&nbsp; <a href="/list">List
				All Books</a>
		</h2>
	</div>

	<div align="center">
		<c:if test="${student != null}">
			<form action="update" method="post">
		</c:if>
		<c:if test="${student == null}">
			<form action="insert" method="post">
		</c:if>

		<table border="1" cellpadding="5">
			<caption>
				<h2>
					<c:if test="${student != null}"> Edit student</c:if>
					<c:if test="${student == null}"> Add new student</c:if>
				</h2>
			</caption>

			<c:if test="${student != null}">
				<input type="hidden" name="rollNo"
					value="<c:out value='${student.rollNo}'/>" />
			</c:if>

			<tr>
				<th>Name :</th>
				<td><input type="text" name="name" size="60"
					value="<c:out value='${student.name}'/>" /></td>
			</tr>

			<tr>
				<th>Branch :</th>
				<td><input type="text" name="branch" size="60"
					value="<c:out value='${student.branch}'/>" /></td>
			</tr>

			<tr>
				<th>Batch :</th>
				<td><input type="text" name="batch" size="60"
					value="<c:out value='${student.batch}'/>" /></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit"
					value="Save" /></td>
			</tr>
		</table>
		</form>
	</div>
</body>
</html>