<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Student Mgmt</title>
</head>
<body>
	<div align="center">
		<h1>Student Management</h1>
		<h2>
			<a href="/xstud/new"> Add new student</a>
			&nbsp;&nbsp;&nbsp;
			<a href="/xstud/list"> List all students</a>
		</h2>
	</div>
	
	<div align="center">
		<table border="1" cellpadding="5">
			<caption><h2>List of students</h2></caption>
			<tr>
				<th>Roll No</th>
				<th>Name</th>
				<th>Branch</th>
				<th>Batch</th>
				<th>Actions</th>
			</tr>
			<c:forEach var="student" items="${studentList}" >
				<tr>
					<td><c:out value="${student.rollNo}"></c:out></td>
					<td><c:out value="${student.name}"></c:out></td>
					<td><c:out value="${student.branch}"></c:out></td>
					<td><c:out value="${student.batch}"></c:out></td>
					<td>
						 <a href="/xstud/edit?idN=<c:out value='${student.rollNo}'/>"> Edit </a> 
						&nbsp;&nbsp;&nbsp;
						<a href="/xstud/delete?idN=<c:out value='${student.rollNo}'/>">Delete </a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>